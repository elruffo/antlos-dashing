require 'net/http'
require 'json'


def get_current_value()
  url = 'http://localhost:1026/v1/contextEntities/type/Searches/id/NumSearches/attributes/number'
  uri = URI(url)
  response = Net::HTTP.get(uri)

  current_value = JSON.parse(response)['attributes'][0]['value']
  return current_value
end

def get_uptimestring()
  url = 'http://localhost:1026/version'
  uri = URI(url)
  response = Net::HTTP.get(uri)

  uptime = JSON.parse(response)['orion']['uptime']
  uptime_data_array = uptime.split(", ").map { |s| s.to_i }
  days = uptime_data_array[0]
  hours = uptime_data_array[1]
  minutes = uptime_data_array[2]
  uptime_string = ""

  if days > 0
    uptime_string = uptime_string + "#{days} days"
  end
  if days > 0 and (hours > 0 or minutes > 0 or seconds > 0)
    uptime_string = uptime_string + ", "
  end
  if hours > 0
    uptime_string = uptime_string + "#{hours} hours"
  end
  if hours > 0 and (minutes > 0 or seconds > 0)
    uptime_string = uptime_string + ", "
  end
  if minutes > 0
    uptime_string = uptime_string + "#{minutes} minutes"
  end
  return uptime_string
end

send_event('valuation', { current_value: get_current_value, last: get_uptimestring })

SCHEDULER.every '60s' do
  send_event('valuation', { last: get_uptimestring })
end
