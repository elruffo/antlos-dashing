class Dashing.Number extends Dashing.Widget
  @accessor 'current', Dashing.AnimatedValue, ->
    if @get('contextResponses')
      current = parseInt(@get('contextResponses')[0]["contextElement"]["attributes"][0]["value"])
    else if @get('current_value')
      current = parseInt(@get('current_value'))

  @accessor 'difference', ->
    last = "In the past " + @get('last')

  onData: (data) ->
    if data.status
      # clear existing "status-*" classes
      $(@get('node')).attr 'class', (i,c) ->
        c.replace /\bstatus-\S+/g, ''
      # add new class
      $(@get('node')).addClass "status-#{data.status}"
